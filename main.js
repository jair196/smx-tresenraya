console.log("Welcome tic tac toe.");

var celda = document.getElementsByClassName('celda');

[].forEach.call(celda, function (el) {
  el.addEventListener("click", whenclick);
  el.addEventListener("dblclick", whendoubleclick);
});

function whenclick() {
  this.innerHTML = "X";
}

function whendoubleclick() {
  this.innerHTML = "O";
}
